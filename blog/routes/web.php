<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@dashboard');
Route::get('/pendaftaran', 'AuthController@daftar');
Route::get('/kirim', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

Route::get('/master', function(){
    return view('layout.master');
});